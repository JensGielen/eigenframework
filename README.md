# Sliderframework JS

Basic sliderframework writen in javascript. Options to set if the slider should play automatic or on manual control, what id to display them in, and able to hide/show controls.

## Getting Started

Download the source files from the repository. Drop them in the main folder of your own project.
Make sure the following files are in your own main folder.

####font-awesome-4.7.0/
####images/
####sliderframework.css
####sliderframework.js



### Prerequisites

Place the following links in your HTML page's head

Link Style sheet to the page

```
<link rel="stylesheet" type="text/css" href="sliderframework.css">
```
Link Font Awesome for icons

```
<link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
```
note: the slider is using font awesome to display the arrow icons, if you are already using a different version of font awesome make sure you link the stylesheet to the correct folder. 

# Example using older version of font awesome

```
<link rel="stylesheet" href="/font-awesome-YOUR-VERSION-NUMBER/css/font-awesome.min.css">
```


### HTML Layout

Get this structure going on your page, the default id for where your images will be displayed is "imagecontainer". You can give this any name your want as long as you call it in the frameworks settings.



```
<body>
   <div id="wrapper">
       <img id="imagecontainer">
       <div id='slfw-next'>
            <i class="fa fa-arrow-right" aria-hidden="true"></i> <!--You can change the icon class with your own icons or image tags-->
       </div>
       <div id='slfw-previous'>
            <i class="fa fa-arrow-left" aria-hidden="true"></i> <!--You can change the icon class with your own icons or image tags-->
        </div>
   </div>
```

### Javascript 

Copy the javascrip in your HTML page. 

```
<script src="sliderframework.js"></script>
    <script>
        var slider = new sliderFramework();

        var settings = {autoplay: true, updateSpeed: 3000, imagecontainer: 'imagecontainer', showcontrols: true};
        var myImages = ['images/black.jpg','images/red.jpg', 'images/blue.jpg','images/yellow.jpg','images/pink.jpg'];
        
        slider.init(myImages, settings);
    </script>
```



## Your own settings

Set your own variables in "var settings" 

Variables     | Type          | Default
------------- | ------------- | --------------
autoplay      | Boolean       | False
updatespeed   | Integer       | 3000 (3 sec.)
imagecontainer| String        | 'imagecontainer'
showcontrols  | Boolean       | true
myImages      | Array         | ['images/black.jpg', 'images/red.jpg'] etc.


var myImages is an array containing default images in the images map, to use your own images replace the links in the array. And dont forget to place the images files in the images/ map. 

# Example 
In this example the autoplay is set to true to overwrite the default value. Also the first 2 images in the myImages array have been set to different values so you can see how to replace the default images with your own.

```
    var settings = {autoplay: true, updateSpeed: 3000, imagecontainer: 'imagecontainer', showcontrols: true};
    var myImages = ['YOURFOLDER/YOURIMAGE.jpg','http://dummyimage.com/dummy', 'images/blue.jpg','images/yellow.jpg','images/pink.jpg'];
```

## DEMO

To watch a live version of the slider framework go to this [DEMO](http://jensgielen.com/codeframework.html)


## Authors

* **Jens Gielen**