
var sliderFramework = function () {
    
    var self = this;

    /**
     * Default settings for the module
     * @type {{selector: string, gravity: number}}
     */
    var defaultSettings = {
        
        updateSpeed: 3000,
        autoplay: false,
        imagecontainer: 'imagecontainer',
        showcontrols: true
    };

   
    var sliderImages = []; // array to be filled with images after init has launched


   // set slider container if not 'imagecontainer'
    var mySlider = document.getElementById(defaultSettings.imagecontainer);
    

   var totalImages = 0; // will be updated in init
   var i = 0;
   // autoplay loop 
   var updateImage = function() {
        
       if(i > (totalImages -1)){
                i = 0;
            }
            mySlider.src = myImages[i];
            i++;
       console.log(i);
    };
    // check if the user has set variables, and replace values if they are not equal to the default settings
    var mergeObjects  = function(object1, object2) {
        for (var attrname in object1) {
            if(object2.hasOwnProperty(attrname)) {
                object1[attrname] = object2[attrname];
            }
        }
    };
    // previous button 
    var prevImage = function (){
            console.log('prev pressed');
        
            if(i == 0){

            }
            else{
                i--;
                mySlider.src = myImages[i];

                console.log(i);
            }
        };
    // next button 
    var nextImage = function () {
        console.log('next pressed');
        if(i > (totalImages -2)){


        }
        else{
            i++;
            mySlider.src = myImages[i];

            console.log(i);

        }

    }
    /**
     * Initializes the module
     * @param {string} [selector] Css selector that targets the element that needs to bounce
     * @param {object} [settings] Object that contain overrides for the default settings
     */
    var init = function(userImages ,userSettings) {
        if(typeof userImages !== 'undefined'){

            sliderImages = userImages; // fill array with userImages
            totalImages = sliderImages.length; //update lenght after filling array

            mergeObjects(defaultSettings, userSettings || {});

            // show first image
            if(totalImages > 0){
               mySlider.src = myImages[0];  
            }
        }else{
            alert('no images set');
        }
            
        
        
        
        
        //manual control
         document.getElementById("slfw-previous").addEventListener("click", prevImage);
        document.getElementById("slfw-next").addEventListener("click", nextImage);
        
        //check if manual control should be display
        if(defaultSettings.showcontrols == false){
            document.getElementById("slfw-previous").style.display = "none";
            document.getElementById("slfw-next").style.display = "none";
        }
        //check if autoplay is true and set timer to interval settings if true
        if(defaultSettings.autoplay == true){
            timer = setInterval(updateImage, defaultSettings.updateSpeed);
            
        }       
        
    };
    

    //Return the functions that should be accessible from the outside. The rest is only accessible from within the object
    return {
        init: init
    };
};